#include<stdio.h>
#include<ctime>
#include<stdlib.h>

int N;
int* pencere()
{
	srand(time(NULL));
	int i;
	int buyukkucuk;
	int *array;
	
	printf("Pencere adedini giriniz : ");
	scanf("%d",&N);
	int dizi[N];
	
	i=0;
	while(i<N){
		buyukkucuk = rand()%2;		
				
		if(buyukkucuk==0) //kucuk karakter
			dizi[i]=rand()%26+97;	 //97-122
		else
			dizi[i]=rand()%26+65;	//65-90
		
		i++;			
	}
	
	array=&dizi[0];
	return array;
}

void yazdir(int* array)
{
	int i;
	int altdeger,ustdeger;
	printf("Alt degeri giriniz : ");
	scanf("%d",&altdeger);
	if(altdeger<0){
			printf("Alt deger 0'dan kucuk olamaz\n");
			exit(1);
	}
	printf("Ust degeri giriniz : ");
	scanf("%d",&ustdeger);
	if(ustdeger>N){
		printf("Pencerenin ust degerinden buyuk deger girdiniz\n");
		exit(1);
	}
	
	for(i=altdeger;i<ustdeger;i++)
		printf("%c  ",*(array+i));
}

int main()
{
	int i;
	
	int *array2 = pencere();
	yazdir(array2);	
		
	
	return 0;
}

/*
 int* pencere fonksiyonu ile istediğimiz adet kadar pencere üretiyor ve bu pencerelere kelimeler yerleştiriyor
 void yazdir fonksiyonu ile istedigimiz aralıktaki pencereleri yazdirir
 */
